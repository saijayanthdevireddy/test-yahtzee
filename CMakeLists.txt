cmake_minimum_required(VERSION 2.8)

project(Yhatzee)
add_executable(yahtzeegame Dice.cpp Die.cpp Game.cpp ScoreCard.cpp YahtzeeProjectClient.cpp)
add_executable(test_scorecard test_scorecard.cpp ScoreCard.cpp)