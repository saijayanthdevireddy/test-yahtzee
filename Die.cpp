#include <iostream>
#include <stdio.h>
#include "Die.h"

//Creates random number from 1 - 6
int Die::roll()
{
    face = rand() % 6 + 1;
    return face;
}

int Die::getFace()
{
    return this->face;
}