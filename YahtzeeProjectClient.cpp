// SE3540 assignment 5.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <time.h>
#include <stdlib.h>
#include "Dice.h"
#include "ScoreCard.h"
#include "Game.h"

int main()
{
    // Seeds the RNG for the dice rolls at the current time
    srand(time(NULL));
    ScoreCard scoreCard = ScoreCard();

    cout << "Press enter to begin playing Yahtzee.";
    cin.ignore();
    scoreCard.print();

    Game myGame = Game();
    myGame.play(scoreCard, nullptr, false);

    cout << "Game Over! Your final score card is displayed above." << endl;
    cout << "Have a good day! :)" << endl;
    cin.ignore();
}
