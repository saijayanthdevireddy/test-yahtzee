#ifndef DICE_H
#define DICE_H

#include "Die.h"

class Dice
{
public:
    //Members
    Die dieOne;
    Die dieTwo;
    Die dieThree;
    Die dieFour;
    Die dieFive;
    // Constructor
    Dice()
    {
        dieOne = Die();
        dieTwo = Die();
        dieThree = Die();
        dieFour = Die();
        dieFive = Die();
        numOfDice = 5;
    }

    //Print function to display the faces of the five dice
    void print();

    //Getter for numOfDice, returns numOfDice
    int getNumDice();

    // To roll the dice
    int *rollAll();

private:
    int numOfDice;
};

#endif