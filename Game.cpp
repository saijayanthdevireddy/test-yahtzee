#include <iostream>
#include "ScoreCard.h"
#include "Dice.h"
#include "Game.h"

//play function starts the game and contains all logic for playing the game to completion.
void Game::play(ScoreCard scoreCard, int *scores, bool bonusYahtzee)
{
    if (scoreCard.isFilled())
        // Game Over
        return;

    if (scores == nullptr)
        scores = dice.rollAll();
    dice.print();

    if (bonusYahtzee)
        cout << "You cannot score into Yahtzee Bonus this roll!" << endl;
    int section = 0;
    cout << "Where would you like to score this roll?: ";
    cin >> section;

    switch (section)
    {
    // Upper Section
    case 1:
        if (scoreCard.getOnes() > -1)
        {
            cout << "The Ones category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.updateOnes(scores);
        break;
    case 2:
        if (scoreCard.getTwos() > -1)
        {
            cout << "The Twos category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.updateTwos(scores);
        break;
    case 3:
        if (scoreCard.getThrees() > -1)
        {
            cout << "The Threes category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.updateThrees(scores);
        break;
    case 4:
        if (scoreCard.getFours() > -1)
        {
            cout << "The Fours category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.updateFours(scores);
        break;
    case 5:
        if (scoreCard.getFives() > -1)
        {
            cout << "The Fives category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.updateFives(scores);
        break;
    case 6:
        if (scoreCard.getSixes() > -1)
        {
            cout << "The Sixes category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.updateSixes(scores);
        break;

    // Lower Section
    // TODO: Noah, can you make your lower section set functions take in
    // all the dice and do the math so it can just take scores, like below.
    case 7:
        if (scoreCard.getThreeOfaKind() != -1)
        {
            cout << "The Three Of a Kind category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.setThreeOfaKind(scores);
        break;
    case 8:
        if (scoreCard.getFourOfaKind() != -1)
        {
            cout << "The Four Of a Kind category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.setFourOfaKind(scores);
        break;
    case 9:
        if (scoreCard.getFullHouse() != -1)
        {
            cout << "The Full House category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.setFullHouse(scores);
        break;
    case 10:
        if (scoreCard.getSmallStraight() != -1)
        {
            cout << "The Small Straight category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.setSmallStraight(scores);
        break;
    case 11:
        if (scoreCard.getlargeStraight() != -1)
        {
            cout << "The Large Straight category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.setLargeStraight(scores);
        break;
    case 12:
        if (scoreCard.getYahtzee() != -1)
        {
            cout << "The Yahtzee category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.setYahtzee(scores);
        break;
    case 13:
        if (scoreCard.getChance() != -1)
        {
            cout << "The Chance category is already taken.\n"
                 << endl;
            return play(scoreCard, scores, false);
        }
        scoreCard.setChance(scores);
        break;
    case 14:
        if(scoreCard.isRollYahtzee(scores))
        { 
            if (scoreCard.getYahtzee() != -1 && !bonusYahtzee)
            {
                scoreCard.addYahtzeeBonus();
                return play(scoreCard, scores, true);
            }
            else if (scoreCard.getYahtzee() == -1)
            {
                cout << "You need to have a Yahtzee on your score card to add a Yahtzee Bonus.\n"
                     << endl;
                return play(scoreCard, scores, false);
            }
            return play(scoreCard, scores, bonusYahtzee);
        }
        else
        {
            cout << "\nYou must have a roll that can qualify as a Yahtzee to score in this category.\n";
            return play(scoreCard, scores, false);
        }
    // Didn't enter in a valid choice
    default:
        return play(scoreCard, scores, false);
    }

    scoreCard.print();

    if (!scoreCard.isFilled())
        // Next Roll
        return play(scoreCard, nullptr, false);
}