#include "Dice.h"
#include <iostream>

//Rolls all of the dice and returns a pointer to the rolls array
int* Dice::rollAll()
{
    static int rolls[5];

    rolls[0] = dieOne.roll();
    rolls[1] = dieTwo.roll();
    rolls[2] = dieThree.roll();
    rolls[3] = dieFour.roll();
    rolls[4] = dieFive.roll();

    return rolls;
}

void Dice::print()
{
    std::cout << " [" << dieOne.getFace() << "] "
        << " [" << dieTwo.getFace() << "] "
        << " [" << dieThree.getFace() << "] "
        << " [" << dieFour.getFace() << "] "
        << " [" << dieFive.getFace() << "] \n"
        << std::endl;
}

//Returns the number of dice in the object
int Dice::getNumDice()
{
    return this->numOfDice;
}