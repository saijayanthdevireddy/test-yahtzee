#ifndef SCORECARD_H
#define SCORECARD_H

#include <iostream>
#include <string>
#include <array>

using namespace std;

struct Score
{
	string name;
	int val;
	bool beenSet;
};

class ScoreCard
{
private:
	const static int NUM_LOWER_SCORES = 9;
	Score lowerSection[NUM_LOWER_SCORES];
	string names[NUM_LOWER_SCORES] = {"Total Score", "Three Of A Kind", "Four Of A Kind", "Full House", "Small Straight",
									  "Large Straight", "Yahtzee", "Chance", "Yahtzee Bonus"};

	int ones, twos, threes, fours, fives, sixes;

public:
	ScoreCard()
	{
		for (int i = 0; i < NUM_LOWER_SCORES; i++)
		{
			lowerSection[i].name = names[i];
			lowerSection[i].beenSet = false;
			lowerSection[i].val = -1;
		}

		ones = -1;
		twos = -1;
		threes = -1;
		fours = -1;
		fives = -1;
		sixes = -1;
	}

	bool isRollYahtzee(int*);

	void setTotalScore();

	int getTotalScore();

	void setThreeOfaKind(int *dice);

	int getThreeOfaKind();

	void setFourOfaKind(int *dice);

	int getFourOfaKind();

	void setFullHouse(int *dice);

	int getFullHouse();

	void setSmallStraight(int *dice);

	int getSmallStraight();

	void setLargeStraight(int *dice);

	int getlargeStraight();

	void setYahtzee(int*);

	int getYahtzee();

	void setChance(int *dice);

	int getChance();

	void addYahtzeeBonus();

	int getYahtzeeBonus();

	int getOnes();

	void updateOnes(int *dice);

	int getTwos();

	void updateTwos(int *dice);

	int getThrees();

	void updateThrees(int *dice);

	int getFours();

	void updateFours(int *dice);

	int getFives();

	void updateFives(int *dice);

	int getSixes();

	void updateSixes(int *dice);

	void updateScore(int scoreToAdd);

	void print();

	bool isFilled();

};

#endif