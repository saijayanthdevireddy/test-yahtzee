#include "ScoreCard.h"
#include <algorithm>

//Sets total score
void ScoreCard::setTotalScore()
{
	int total = 0;

	for (int i = 1; i < NUM_LOWER_SCORES; i++)
	{
		if (lowerSection[i].beenSet == true)
		{
			total += lowerSection[i].val;
		}
	}

	if (ones != -1)
	{
		total += ones;
	}
	if (twos != -1)
	{
		total += twos;
	}
	if (threes != -1)
	{
		total += threes;
	}
	if (fours != -1)
	{
		total += fours;
	}
	if (fives != -1)
	{
		total += fives;
	}
	if (sixes != -1)
	{
		total += sixes;
	}

	if (ones + twos + threes + fours + fives + sixes >= 63)
		total += 35;

	lowerSection[0].val = total;
	lowerSection[0].beenSet = true;
}

bool ScoreCard::isRollYahtzee(int* dice)
{
	return (dice[0] == dice[1] == dice[2] == dice[3] == dice[4]);
}

//Returns total score
int ScoreCard::getTotalScore()
{
	return lowerSection[0].val;
}

//Sets three of a kind. Param is val that is multiplied 3 times.
void ScoreCard::setThreeOfaKind(int *dice)
{
	int diceArray[5];
	for(int i = 0; i < 5; i++)
	{
		diceArray[i] = *dice;
		dice++;
	}
	dice = dice-5;
	sort(diceArray, diceArray + 5);
	
	for(int i = 0; i <= 2; i++)
	{
		if(diceArray[i] == diceArray[i+1] && diceArray[i+1] == diceArray[i+2])
		{
			int score = 0;
			for (int j = 1; j <= 5; j++)
			{
				score += *dice;
				dice++;
			}
			lowerSection[1].val = score;
			lowerSection[1].beenSet = true;
			setTotalScore();
			return;
		}
	}
	lowerSection[1].val = 0;
	lowerSection[1].beenSet = true;
	setTotalScore();
}

//Returns 3 of a kind value -1 if not set.
int ScoreCard::getThreeOfaKind()
{
	return lowerSection[1].val;
}

//Sets four of a kind. Param is val that is multipled 4 times
void ScoreCard::setFourOfaKind(int *dice)
{
	int diceArray[5];
	for(int i = 0; i < 5; i++)
	{
		diceArray[i] = *dice;
		dice++;
	}
	dice = dice-5;
	sort(diceArray, diceArray + 5);

	for(int i = 0; i <= 1; i++)
	{
		if(diceArray[i] == diceArray[i+1] && diceArray[i+1] == diceArray[i+2] && diceArray[i+2] == diceArray[i+3])
		{
			int score = 0;
			for (int j = 1; j <= 5; j++)
			{
				score += *dice;
				dice++;
			}
			lowerSection[2].val = score;
			lowerSection[2].beenSet = true;
			setTotalScore();
			return;
		}
	}
	lowerSection[2].val = 0;
	lowerSection[2].beenSet = true;
	setTotalScore();
}

//Returns 4 of a kind value -1 if not set.
int ScoreCard::getFourOfaKind()
{
	return lowerSection[2].val;
}

//Sets Full house to default value of 25
void ScoreCard::setFullHouse(int *dice)
{
	
	lowerSectionnn[3].val = 25;
	lowerSection[3].beenSet = true;
	setTotalScore();
}

//Returns Full House value or -1 if not set.
int ScoreCard::getFullHouse()
{
	return lowerSection[3].val;
}

//Sets small straight default value
void ScoreCard::setSmallStraight(int *dice)
{
	int diceArray[5];
	for(int i = 0; i < 5; i++)
	{
		diceArray[i] = *dice;
		dice++;
	}
	dice = dice - 5;
	sort(diceArray, diceArray + 5);

	for(int i = 0; i <= 1; i++)
	{
		if(diceArray[i]+1 == diceArray[i+1] && diceArray[i+1]+1 == diceArray[i+2] && diceArray[i+2]+1 == diceArray[i+3] )
		{
			lowerSection[4].val = 30;
			lowerSection[4].beenSet = true;
			setTotalScore();
			return;
		}
	}

	lowerSection[4].val = 0;
	lowerSection[4].beenSet = true;
	setTotalScore();

	
}

//Return small straight value or -1 if not set.
int ScoreCard::getSmallStraight()
{
	return lowerSection[4].val;
}

//Sets large straight default value
void ScoreCard::setLargeStraight(int *dice)
{
	int diceArray[5];
	for(int i = 0; i < 5; i++)
	{
		diceArray[i] = *dice;
		dice++;
	}
	dice = dice - 5;
	sort(diceArray, diceArray + 5);


	if((diceArray[0] == 1 && diceArray[1] == 2 && diceArray[2] == 3 && diceArray[3] == 4 && diceArray[4] == 5)
	 || (diceArray[0] == 2 && diceArray[1] == 3 && diceArray[2] == 4 && diceArray[3] == 5 && diceArray[4] == 6))
	{
		lowerSection[5].val = 40;
		lowerSection[5].beenSet = true;
		setTotalScore();
		return;
	}


	lowerSection[5].val = 0;
	lowerSection[5].beenSet = true;
	setTotalScore();
}

//Return Large straight value or -1 if not set.
int ScoreCard::getlargeStraight()
{
	return lowerSection[5].val;
}

//Sets the default Yahtzee value
void ScoreCard::setYahtzee(int* dice)
{
	if (isRollYahtzee(dice))
	{
		lowerSection[6].val = 50;
		lowerSection[6].beenSet = true;
		setTotalScore();
	}
	else
	{
		lowerSection[6].val = 0;
		lowerSection[6].beenSet = true;
		setTotalScore();
	}
	
}

//Retuns Yahtzee value or -1 if not set.
int ScoreCard::getYahtzee()
{
	return lowerSection[6].val;
}

//Set chance value. Parameters are the 5 die that need input.
void ScoreCard::setChance(int *dice)
{
	int score = 0;
	for (int i = 1; i <= 5; i++)
	{
		score += *dice;
		dice++;
	}
	lowerSection[7].beenSet = true;
	lowerSection[7].val = score;
	setTotalScore();
}

//Reutns chance value or -1 if not set.
int ScoreCard::getChance()
{
	return lowerSection[7].val;
}

//Adds anothe Yahtzee bonus to score
void ScoreCard::addYahtzeeBonus()
{
	if (lowerSection[8].beenSet == false)
	{
		lowerSection[8].val = 100;
		lowerSection[8].beenSet = true;
	}
	else
	{
		lowerSection[8].val += 100;
	}
	setTotalScore();
}

//Gets the Yahtzee bonus values
int ScoreCard::getYahtzeeBonus()
{
	return lowerSection[8].val;
}

int ScoreCard::getOnes()
{
	return ones;
}

void ScoreCard::updateOnes(int *dice)
{
	int score = 0;
	for (int i = 1; i <= 5; i++)
	{
		if (*dice == 1)
			score += *dice;
		dice++;
	}
	ones = score;
	setTotalScore();
}

int ScoreCard::getTwos()
{
	return twos;
}

void ScoreCard::updateTwos(int *dice)
{
	int score = 0;
	for (int i = 1; i <= 5; i++)
	{
		if (*dice == 2)
			score += *dice;
		dice++;
	}
	twos = score;
	setTotalScore();
}

int ScoreCard::getThrees()
{
	return threes;
}

void ScoreCard::updateThrees(int *dice)
{
	int score = 0;
	for (int i = 1; i <= 5; i++)
	{
		if (*dice == 3)
			score += *dice;
		dice++;
	}
	threes = score;
	setTotalScore();
}

int ScoreCard::getFours()
{
	return fours;
}

void ScoreCard::updateFours(int *dice)
{
	int score = 0;
	for (int i = 1; i <= 5; i++)
	{
		if (*dice == 4)
			score += *dice;
		dice++;
	}
	fours = score;
	setTotalScore();
}

int ScoreCard::getFives()
{
	return fives;
}

void ScoreCard::updateFives(int *dice)
{
	int score = 0;
	for (int i = 1; i <= 5; i++)
	{
		if (*dice == 5)
			score += *dice;
		dice++;
	}
	fives = score;
	setTotalScore();
}

int ScoreCard::getSixes()
{
	return sixes;
}

void ScoreCard::updateSixes(int *dice)
{
	int score = 0;
	for (int i = 1; i <= 5; i++)
	{
		if (*dice == 6)
			score += *dice;
		dice++;
	}
	sixes = score;
	setTotalScore();
}

void ScoreCard::print()
{
	cout << "\nUpper Section:" << endl;

	int subScore = 0;
	if (ones > -1)
	{
		cout << "[1] - Ones: " << ones << endl;
		subScore += ones;
	}
	else
		cout << "[1] - Ones: " << endl;

	if (twos > -1)
	{
		cout << "[2] - Twos: " << twos << endl;
		subScore += twos;
	}
	else
		cout << "[2] - Twos: " << endl;

	if (threes > -1)
	{
		cout << "[3] - Threes: " << threes << endl;
		subScore += threes;
	}
	else
		cout << "[3] - Threes: " << endl;

	if (fours > -1)
	{
		cout << "[4] - Fours: " << fours << endl;
		subScore += fours;
	}
	else
		cout << "[4] - Fours: " << endl;

	if (fives > -1)
	{
		cout << "[5] - Fives: " << fives << endl;
		subScore += fives;
	}
	else
		cout << "[5] - Fives: " << endl;

	if (sixes > -1)
	{
		cout << "[6] - Sixes: " << sixes << endl;
		subScore += sixes;
	}
	else
		cout << "[6] - Sixes: " << endl;

	cout << "Upper Sub-Score: " << subScore << endl;

	if (ones + twos + threes + fours + fives + sixes >= 63)
		cout << "Upper Score Bonus (Over 63): 35" << endl;
	else
		cout << "Upper Score Bonus (Over 63): 0" << endl;

	cout << "\nLower Section: " << endl;

	for (int i = 1; i < NUM_LOWER_SCORES; i++)
	{
		cout << "[" << 6 + i << "] - " << lowerSection[i].name + ": ";
		if (lowerSection[i].beenSet == false)
		{
			cout << "  ";
		}
		else
		{
			cout << lowerSection[i].val;
		}
		cout << endl;
	}

	cout << "\n"
		 << lowerSection[0].name + ": ";
	if (lowerSection[0].beenSet == true)
	{
		cout << getTotalScore() << endl;
	}
	else
	{
		cout << 0 << endl;
	}
}

bool ScoreCard::isFilled()
{
	if (!(ones > -1 && twos > -1 && threes > -1 && fours > -1 &&
		  fives > -1 && sixes > -1))
		return false;
	
	cout << "Upper Section is Filled!" << endl;

	int i = 0;
	for (Score score : lowerSection)
	{
		i++;
		if (i == 0 || i == 9)
			continue;
		if (!score.beenSet)
			return false;
	}
	
	cout << "Lower Section is Filled!" << endl;

	return true;
}
