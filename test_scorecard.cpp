#include <iostream>
#include <cassert>

#include "ScoreCard.h"

int main(int argc, char * argv[]) {

    // example test case
    {
        // setup
        ScoreCard scoreCard = ScoreCard();
        int rolls[5];
        rolls[0] = 1;
        rolls[1] = 2;
        rolls[2] = 1;
        rolls[3] = 2;
        rolls[4] = 2;
        scoreCard.setFullHouse(rolls);
        int fullHouse = scoreCard.getFullHouse();
        assert(fullHouse == 25);

    }

    return 0;
}
