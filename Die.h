#ifndef DIE_H
#define DIE_H

class Die
{
public:
    // Constructor
    Die()
    {
        face = -1;
    }

    // To roll the die
    int roll();

    // To get the face of the die
    int getFace();

private:
    int face;
};

#endif