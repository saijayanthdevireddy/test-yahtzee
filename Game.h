#ifndef GAME_H
#define GAME_H

#include "Dice.h"

class Game
{
public:
    Dice dice = Dice();

    void play(ScoreCard scoreCard, int *scores, bool bonusYahtzee);
};

#endif